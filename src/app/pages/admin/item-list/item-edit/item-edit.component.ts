import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { Photo } from 'src/app/model/photo';
import { ItemService } from 'src/app/service/item.service';
import { PhotoUploadService } from 'src/app/service/photo-upload.service';
import { CroppedEvent } from 'ngx-photo-editor';
import { CategoryService } from 'src/app/service/category.service';
import { Category } from 'src/app/model/category';
import { TechService } from 'src/app/service/tech.service';
import { Tech } from 'src/app/model/tech';

@Component({
  selector: 'app-item-edit',
  templateUrl: './item-edit.component.html',
  styleUrls: ['./item-edit.component.scss'],
})
export class ItemEditComponent implements OnInit, OnDestroy {
  //FOR CROPPER
  imageChangedEvent: any;
  base64: any;
  //
  full: boolean;
  preview: boolean;
  more: boolean;

  list$: Photo[];
  categoryOpt: Category[] = [];
  techOpt: Tech[] = [];
  subCatOpt: string[] = ['cities', 'christmas', 'other'];
  selectedFiles: any;
  currentPhoto: Photo;
  selectedPreviewFiles: any;
  currentPreviewPhoto: Photo;
  imageURLs: string[] = [];
  extraImageUrls: string[] = [];
  imageNames: string[] = [];
  imageIds: string[] = [];
  progress: number;
  newCat: boolean = false;
  newTech: boolean = false;
  isbag: boolean;
  isGreet: boolean;
  subscription: Subscription = new Subscription();

  form = new FormGroup({
    id: new FormControl({ value: '', disabled: true }),
    name: new FormControl('', [Validators.required, Validators.maxLength(40)]),
    description: new FormControl('', Validators.maxLength(400)),
    category: new FormControl('', Validators.required),
    subCategory: new FormControl(''),
    tech: new FormControl(''),
    images: new FormControl(''),
    extraImages: new FormControl(''),
    mainImage: new FormControl(''),
    mainFullImage: new FormControl(''),
    date: new FormControl(''),
    imageId: new FormControl(''),
    imageName: new FormControl(''),
    newCategory: new FormControl(''),
    newTech: new FormControl(''),
    instaLink: new FormControl(''),
    size: new FormControl(''),
  });

  constructor(
    private iService: ItemService,
    private phUService: PhotoUploadService,
    private dialogRef: MatDialogRef<ItemEditComponent>,
    private catService: CategoryService,
    private techService: TechService,
    @Inject(MAT_DIALOG_DATA) data: any
  ) {
    this.form.patchValue(data);
    if (data.id != '0' && data.images != '' && data.images != undefined) {
      this.imageURLs = data.images;
    }
    if (
      data.id != '0' &&
      data.extraImages != '' &&
      data.extraImages != undefined
    ) {
      this.extraImageUrls = data.extraImages;
    }
    if (data.id != '0' && data.imageName != '' && data.imageName != undefined) {
      this.imageNames = data.imageName;
    }
    if (data.category == 'bags') {
      this.isbag = true;
    } else if (data.category == 'greet-print') {
      this.isGreet = true;
    }
  }

  ngOnInit(): void {
    this.form.get('newCategory').reset();
    this.form.get('newTech').reset();
    this.catService.getAll();
    this.techService.getAll();
    this.catService.list$.subscribe((list) => (this.categoryOpt = list));
    this.techService.list$.subscribe((list) => (this.techOpt = list));
    this.phUService.list$.subscribe((list) => (this.list$ = list));
    this.phUService.progress.subscribe((value) => (this.progress = value));
  }

  onSubmit(): void {
    if (this.form.get('id')?.value == '0') {
      this.form.patchValue({
        imageId: this.imageIds,
        imageName: this.imageNames,
        images: this.imageURLs,
        extraImages: this.extraImageUrls,
      });
      this.iService.create(this.form.value);
      this.dialogRef.close();
      this.progress = 0;
    } else {
      this.form.patchValue({
        images: this.imageURLs,
        imageName: this.imageNames,
        extraImages: this.extraImageUrls,
      });
      this.iService.update(this.form.getRawValue());
      this.dialogRef.close();
    }
  }
  onDelete(name: string, index?: any): void {
    if (confirm()) {
      if (this.imageURLs[index] == this.form.get('mainImage').value) {
        this.form.get('mainImage').reset();
      } else if (
        this.imageURLs[index] == this.form.get('mainFullImage').value
      ) {
        this.form.get('mainFullImage').reset();
      }
      this.phUService.deleteImageFrStAndDB(name, '');
      this.extraImageUrls = this.extraImageUrls.filter(
        (url) => url != this.imageURLs[index]
      );
      this.imageURLs.splice(index, 1);
      this.imageNames.splice(index, 1);
      this.form.patchValue({
        images: this.imageURLs,
        imageName: this.imageNames,
        extraImages: this.extraImageUrls,
      });
      this.iService.update(this.form.getRawValue());
    }
  }

  onUpload(type: 'preview' | 'main' | 'more'): void {
    if (type == 'preview') {
      const file = this.selectedPreviewFiles;
      this.selectedPreviewFiles = undefined;
      this.currentPreviewPhoto = new Photo(file);
      this.phUService.pushFileToStorage(this.currentPreviewPhoto);
      this.subscription = this.phUService.image$.subscribe((image) => {
        if (image != null) {
          this.form.patchValue({ mainImage: image.url });
          this.imageIds.push(image.key);
          this.imageNames.push(image.name);
          this.imageURLs.push(image.url);
          this.phUService.image$.next(null);
          this.subscription.unsubscribe();
        }
      });
    } else if (type == 'main') {
      const file = this.selectedFiles;
      this.selectedFiles = undefined;
      this.currentPhoto = new Photo(file[0]);
      this.phUService.pushFileToStorage(this.currentPhoto);
      this.subscription = this.phUService.image$.subscribe((image) => {
        if (image != null) {
          this.form.patchValue({ mainFullImage: image.url });
          this.imageIds.push(image.key);
          this.imageNames.push(image.name);
          this.imageURLs.push(image.url);
          this.phUService.image$.next(null);
          this.subscription.unsubscribe();
        }
      });
    } else if (type == 'more') {
      const file = this.selectedFiles;
      this.selectedFiles = undefined;
      this.currentPhoto = new Photo(file[0]);
      this.phUService.pushFileToStorage(this.currentPhoto);
      this.subscription = this.phUService.image$.subscribe((image) => {
        if (image != null) {
          this.imageIds.push(image.key);
          this.imageNames.push(image.name);
          this.imageURLs.push(image.url);
          this.extraImageUrls.push(image.url);
          this.phUService.image$.next(null);
          this.subscription.unsubscribe();
        }
      });
    }
  }
  fileChangeEvent(event: any, main: boolean): void {
    if (main == true) {
      this.imageChangedEvent = event;
    } else {
      this.selectedFiles = event.target.files;
    }
  }

  imageCropped(event: CroppedEvent): void {
    this.selectedPreviewFiles = event.file;
  }

  onCatChange() {
    if (this.form.get('category').value == 'bags') {
      this.isbag = true;
      this.isGreet = false;
    } else if (this.form.get('category').value == 'greet-print') {
      this.isGreet = true;
      this.isbag = false;
    } else {
      this.isbag = false;
      this.isGreet = false;
    }
  }

  createNewCat(): void {
    if (this.form.get('newCategory').value != null) {
      this.catService.create({
        id: Math.random()
          .toString(36)
          .replace(/[^a-zA-Z0-9]+/g, '')
          .substr(2, 10),
        name: this.form.get('newCategory').value,
        description: '',
      });
      this.catService.getAll();
      this.form.get('newCategory').reset();
      this.newCat = false;
    } else {
      return;
    }
  }
  createNewTech(): void {
    if (this.form.get('newTech').value != null) {
      this.techService.create({
        id: Math.random()
          .toString(36)
          .replace(/[^a-zA-Z0-9]+/g, '')
          .substr(2, 10),
        name: this.form.get('newTech').value,
      });
      this.techService.getAll();
      this.form.get('newTech').reset();
      this.newTech = false;
    } else {
      return;
    }
  }

  close(): void {
    this.dialogRef.close();
  }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
