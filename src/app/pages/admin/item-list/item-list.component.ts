import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Observable, Subscription } from 'rxjs';
import { Item } from 'src/app/model/item';
import { ItemService } from 'src/app/service/item.service';
import { PhotoUploadService } from 'src/app/service/photo-upload.service';
import { DeleteModalComponent } from '../delete-modal/delete-modal.component';
import { ItemEditComponent } from './item-edit/item-edit.component';

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.scss'],
})
export class ItemListComponent implements OnInit, OnDestroy {
  item: Item = {
    id: '0',
    name: '',
    description: '',
    category: '',
    tech: '',
    images: '',
    date: '',
    mainImage: '',
    mainFullImage: '',
    imageId: '',
    imageName: '',
    instaLink: ''
  };
  subscription: Subscription = new Subscription();
  @ViewChild('paginator') paginator: MatPaginator;
  dataSource: MatTableDataSource<any> = new MatTableDataSource<any>();
  columns: string[] = ['name', 'category',  'date', 'mainImage', 'edit'];
  form = new FormGroup({
    filterKey:  new FormControl('')
  })

  constructor(
    private iService: ItemService,
    private dialog: MatDialog,
    private phUService: PhotoUploadService,
  ) {}

  ngOnInit(): void {
    this.subscription = this.iService.list$.subscribe((list) => {
      this.dataSource = new MatTableDataSource(list);
      this.dataSource.paginator = this.paginator;
      this.dataSource.filterPredicate = function (record, filter) {
        return record.category.toLowerCase().includes(filter.toLocaleLowerCase());
      };
    });
    this.iService.getAll();
  }

  openDialog(doc: any, comp?: 'del' | 'edit'): void {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = doc;
    dialogConfig.maxWidth = '100%';
    if(comp != 'del'){
      this.dialog.open(ItemEditComponent, dialogConfig);
    } else {
      this.dialog.open(DeleteModalComponent, dialogConfig)
    }

  }

  applyFilter(): void {
    const filterValue = this.form.get('filterKey').value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
