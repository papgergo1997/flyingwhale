import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Item } from 'src/app/model/item';
import { ItemService } from 'src/app/service/item.service';
import { PhotoUploadService } from 'src/app/service/photo-upload.service';

@Component({
  selector: 'app-delete-modal',
  templateUrl: './delete-modal.component.html',
  styleUrls: ['./delete-modal.component.scss'],
})
export class DeleteModalComponent implements OnInit {
  item: Item;

  constructor(
    private iService: ItemService,
    private phUService: PhotoUploadService,
    private dialogRef: MatDialogRef<DeleteModalComponent>,
    @Inject(MAT_DIALOG_DATA) data: any
  ) {
    this.item = data;
  }

  ngOnInit(): void {}

  onDelete() {
    if (this.item.imageName != undefined) {
      this.iService.delete(this.item);
      this.item.imageName.map((name) => {
        this.phUService.deleteImageFrStAndDB(name, '');
      });
      this.dialogRef.close();
    } else{
      this.iService.delete(this.item);
      this.dialogRef.close();
    }
    console.log(this.item.imageName)
  }

  close() {
    this.dialogRef.close();
  }
}
