import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/service/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit, OnDestroy {
  currentUser: any;
  loginForm = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required]),
  });
  errorMessage: string = '';
  isLoading: boolean = false;
  error: string;
  subscription: Subscription = new Subscription();

  constructor(
    private authService: AuthService,
    private router: Router,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.subscription = this.authService.currentUser.subscribe(
      (user) => (this.currentUser = user)
    );
  }

  login() {
    this.isLoading = true;
    this.authService
      .login(
        this.loginForm.get('email').value,
        this.loginForm.get('password').value
      )
      .subscribe(
        (resData) => {
          this.isLoading = false;
          this.snackBar.open('Successful Login!', 'Close', {
            duration: 2000,
            panelClass: 'snack',
            horizontalPosition: 'right',
          });
          this.router.navigate(['main/about']);
        },
        (errorMessage) => {
          this.snackBar.open(errorMessage, 'Close', {
            duration: 3000,
            panelClass: 'snackError',
          });
          this.error = errorMessage;
          this.isLoading = false;
        }
      );
  }
  ngOnDestroy(): void {
      this.subscription.unsubscribe()
  }
}
