export interface Item {
  id: string;
  name: string;
  description: string;
  category: string;
  subCategory?: string;
  date: string;
  tech: string;
  images: string;
  extraImages?: string;
  mainImage: string;
  mainFullImage: string;
  imageId: any;
  imageName: any;
  instaLink: string;
  size?: string;
}
