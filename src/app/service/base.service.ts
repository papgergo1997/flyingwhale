import { HttpClient } from '@angular/common/http';
import { Inject, Injectable, OnDestroy } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { exhaustMap, map, take } from 'rxjs/operators';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root',
})
export class BaseService<T extends { id: string }> implements OnDestroy {
  list$: BehaviorSubject<T[]> = new BehaviorSubject<T[]>([]);
  subscription: Subscription = new Subscription();
  URL: string = '';
  currentUser: any;
  baseURL: string =
    'https://flyingwhale-625ae-default-rtdb.europe-west1.firebasedatabase.app';

  constructor(
    public http: HttpClient,
    public authService: AuthService,
    public snackBar: MatSnackBar,
    @Inject('entityName') entityName: string
  ) {
    this.URL = `${this.baseURL}/${entityName}`;
    this.currentUser = JSON.parse(localStorage.getItem('user'));
  }

  getAll(): void {
    this.subscription = this.http
      .get(`${this.URL}.json`)
      .pipe(
        map((resp) => {
          const arr = [];
          for (const key in resp) {
            if (resp.hasOwnProperty(key)) {
              arr.push({ ...resp[key], id: key });
            }
          }
          return arr;
        })
      )
      .subscribe((list) => this.list$.next(list));
  }

  get(id: string): Observable<any> {
    return this.http.get(`${this.URL}/${id}.json`);
  }

  create(doc: T): void {
    if (this.currentUser != null) {
      this.subscription = this.http
        .post<T>(`${this.URL}.json?auth=${this.currentUser._token}`, doc)
        .subscribe(() => {
          this.snackBar.open('Successfully created!', 'Close', {
            duration: 2000,
            panelClass: 'snack',
          });
          this.getAll();
        });
    } else {
      this.subscription = this.authService.currentUser
        .pipe(
          take(1),
          exhaustMap((user) => {
            return this.http.post<T>(
              `${this.URL}.json?auth=${user.token}`,
              doc
            );
          })
        )
        .subscribe(() => {
          this.snackBar.open('Successfully created!', 'Close', {
            duration: 2000,
            panelClass: 'snack',
          });
          this.getAll();
        });
    }
  }

  update(doc: T): void {
    if (this.currentUser != null) {
      this.subscription = this.http
        .patch(
          `${this.URL}/${doc.id}.json?auth=${this.currentUser._token}`,
          doc
        )
        .subscribe(() => {
          this.snackBar.open('Successfully updated!', 'Close', {
            duration: 2000,
            panelClass: 'snack',
          });
          this.getAll();
        });
    } else {
      this.subscription = this.authService.currentUser
        .pipe(
          take(1),
          exhaustMap((user) => {
            return this.http.patch<T>(
              `${this.URL}/${doc.id}.json?auth=${user.token}`,
              doc
            );
          })
        )
        .subscribe(() => {
          this.snackBar.open('Successfully updated!', 'Close', {
            duration: 2000,
            panelClass: 'snack',
          });
          this.getAll();
        });
    }
  }

  delete(doc: T): void {
    if (this.currentUser != null) {
      this.subscription = this.http
        .delete(`${this.URL}/${doc.id}.json?auth=${this.currentUser._token}`)
        .subscribe(() => {
          this.snackBar.open('Successfully deleted!', 'Close', {
            duration: 2000,
            panelClass: 'snack',
          });
          this.getAll();
        });
    } else {
      this.subscription = this.authService.currentUser
        .pipe(
          take(1),
          exhaustMap((user) => {
            return this.http.delete<T>(
              `${this.URL}/${doc.id}.json?auth=${user.token}`
            );
          })
        )
        .subscribe(() => {
          this.snackBar.open('Successfully deleted!', 'Close', {
            duration: 2000,
            panelClass: 'snack',
          });
          this.getAll();
        });
    }
  }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
